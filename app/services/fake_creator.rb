class FakeCreator
  def call
    ActiveRecord::Base.transaction do
      tournament = new_tournament
      build_teams_for(tournament)
      tournament.save!
      tournament.start!
    end
  end

  private

  def new_tournament
    Tournament.new(tournament_attributes)
  end

  def build_teams_for(tournament)
    Tournament::SCALE.times { tournament.teams.build(team_attributes) }
  end

  def tournament_attributes
    {
        name: Faker::Football.unique.competition
    }
  end

  def team_attributes
    {
        name: Faker::Football.team + Faker::Number.unique.number(3)
    }
  end
end

class GameScoresGenerator
  def self.call(games)
    games.each do |game|
      game.participants.first.update(score: rand(3))
      game.participants.last.update(score: rand(3..6))
    end
  end
end

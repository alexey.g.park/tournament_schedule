module GamesCreator
  class BaseGames
    def initialize(teams, group)
      @teams = teams
      @group = group
    end

    def creating
      raise NotImplementedError
    end

    private

    def create_game(group, teams)
      game = group.games.create!
      game.teams << teams
    end
  end
end


module GamesCreator
  class DivisionGames < BaseGames

    def creating
      @teams.to_a.combination(2).to_a.each do |pair_of_teams|
        create_game(@group, pair_of_teams)
      end
    end
  end
end

module GamesCreator
  class PlayOffGames < BaseGames

    def creating
      ordered_teams = @teams.order(score: :desc).to_a
      while ordered_teams.size > 0
        create_game(@group, [ordered_teams.shift, ordered_teams.pop])
      end
    end
  end

  class FinalGames < PlayOffGames; end
end

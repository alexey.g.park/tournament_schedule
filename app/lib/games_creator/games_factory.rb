module GamesCreator
  class GamesFactory
    attr_reader :teams, :group, :factory_type
    def initialize(teams, group, factory_type = nil)
      @teams = teams
      @group = group
      @factory_type = factory_type
    end

    def action
      define_factory_class.new(teams, group).creating
    end

    private

    def define_factory_class
      raise 'factory_type is not implemented' if factory_type.nil?
      ('GamesCreator::' + factory_type.camelize + 'Games').constantize
    end
  end
end

module WelcomeHelper
  def group_can_be_generated?(tournament)
    tournament.groups.first.try(:games).try(:size) != 1 and tournament.groups.first(2).all?(&:finished?)
  end
end

class Team < ApplicationRecord
  belongs_to :tournament
  has_and_belongs_to_many :groups
  has_many :participants, dependent: :destroy
  has_many :games, through: :participants

  # When the team wins a game it gets 3 points
  def get_winner_score
    self.score += 3
    save!
  end

  # When the team played a draw it gets 1 point
  def get_draw_score
    self.score += 1
    save!
  end

end

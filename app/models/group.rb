class Group < ApplicationRecord
  has_and_belongs_to_many :teams
  has_many :games, dependent: :destroy
  belongs_to :tournament

  enum stage: %i(division play_off final)

  def best_four_teams
    teams.order(score: :desc).first(4)
  end

  def finished?
    games.any?(&:finished?)
  end

  def winners
    games.map(&:winner_team)
  end

end

class Participant < ApplicationRecord
  after_update :distribute_game_scores, if: Proc.new { |participant| participant.game.finished? }
  belongs_to :game
  belongs_to :team

  def opponent
    game.participants.where.not(participant: self).first
  end

  def distribute_game_scores
    game.distribute_scores
  end
end

class Game < ApplicationRecord

  TEAMS_IN_GAME = 2.freeze

  belongs_to :group
  has_many :participants, dependent: :destroy
  has_many :teams, through: :participants

  def finished?
    participants.pluck(:score).compact.size == TEAMS_IN_GAME
  end

  def winner_team
    participants.max_by(&:score).team
  end

  def draw?
    participants.first.score == participants.last.score
  end

  def distribute_scores
    draw? ? distribute_draw_scores : distribute_winner_scores
  end

  private

  def distribute_draw_scores
    participants.each { |p| p.team.get_draw_score }
  end

  def distribute_winner_scores
    winner_team.get_winner_score
  end
end

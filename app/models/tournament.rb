class Tournament < ApplicationRecord
  include AASM

  SCALE = 16.freeze
  DIVISIONS_SCALE = 8.freeze
  DIVISION_NAMES = %w(Division\ A Division\ B).freeze
  PLAY_OFF_NAMES = %w(Play_off).freeze
  FINAL_NAMES = %w(1/2 Super\ Final).freeze

  has_many :teams, dependent: :destroy
  has_many :groups, dependent: :destroy
  accepts_nested_attributes_for :teams, reject_if: lambda { |team| team[:name].blank? }, allow_destroy: true

  validates :name, presence: true
  validate :check_teams_number

  aasm column: 'stage' do
    state :initial, initial: true
    state :division
    state :play_off
    state :final

    event :start do
      transitions from: :initial, to: :division
    end
    event :to_play_off do
      transitions from: :division, to: :play_off
    end
    event :to_final do
      transitions from: :play_off, to: :final
    end
  end

  def divisions
    groups.where(stage: Group.stages[:division])
  end

  def current_groups
    current_stage_groups.reject(&:finished?)
  end

  def current_stage_groups
    groups.where(stage: Group.stages[stage.to_sym])
  end

  private

  def teams_count_valid?
    teams.size == SCALE
  end

  def check_teams_number
      errors.add(:base, 'teams scale must be equal to', count: SCALE) unless teams_count_valid?
  end
end

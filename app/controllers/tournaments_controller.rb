class TournamentsController < ApplicationController
  before_action :set_tournament, only: :show

  def index
    @tournaments = Tournament.all
  end

  def new
    @tournament = Tournament.new
  end

  def create
    @tournament = Tournament.new(tournament_params)

    respond_to do |format|
      if @tournament.save
        @tournament.start!
        format.html { redirect_to root_path, notice: 'Tournament was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end

  private

  def set_tournament
    @tournament = Tournament.find(params[:id])
  end

  def tournament_params
    params.require(:tournament).permit(:name, teams_attributes: [:id, :name, :_destroy])
  end
end

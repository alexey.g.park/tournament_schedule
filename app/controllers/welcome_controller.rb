class WelcomeController < ApplicationController
  def index
    @tournament = Tournament.includes(groups: { games: { participants: :team } })
                            .order('groups.id')
                            .includes(:teams)
                            .last
  end
end

class FakesController < ApplicationController
  def create
    FakeCreator.new.call
    redirect_to root_path
  end
end

class GroupsController < ApplicationController
  def create
    @tournament = Tournament.find(params[:tournament_id])
    result = StageCreator.call(division_stage_packet)
    if result.success?
      redirect_to root_path, notice: 'Groups were successfully created.'
    else
      logger.info result.message
      redirect_to root_path, notice: 'Groups were not created. Things look blue'
    end
  end

  def generate_results
    @group = Group.find(params[:id])
    GameScoresGenerator.call(@group.games)
    redirect_to root_path, notice: 'Results were successfully generated.'
  end

  private

  def division_stage_packet
    {
        tournament: @tournament
    }
  end
end

class CleanController < ApplicationController
  def destroy
    ApplicationRecord.transaction do
      Tournament.destroy_all
      Team.destroy_all
    end
    redirect_to root_path, notice: 'Tournament and its data have been successfully destroyed.'
  end
end

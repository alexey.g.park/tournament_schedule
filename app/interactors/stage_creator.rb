class StageCreator
  include Interactor

  def call
    validate_tournament_stage
    create_groups_by_stage
    create_games
    next_tournament_stage
  end

  private

  def validate_tournament_stage
    unless next_stage_accepted?(context.tournament)
      context.fail!(message: "Can not generate next stage unless previous is not finished!")
    end
  end

  def create_groups_by_stage
    send 'create_' + context.tournament.stage.pluralize
  rescue StandardError => e
    context.fail!(message: "Can not create groups by stage. Error: #{e}")
  end

  def create_games
    context.tournament.current_groups.each do |group|
      GamesCreator::GamesFactory.new(group.teams, group, context.tournament.stage).action
    end
  rescue StandardError => e
    context.fail!(message: "Can not create games. Error: #{e}")
  end

  def next_tournament_stage
    context.tournament.to_final! if context.tournament.play_off?
    context.tournament.to_play_off! if context.tournament.division?
  end

  def create_divisions
    context.tournament.teams.shuffle.each_slice(Tournament::DIVISIONS_SCALE) do |team_group|
      stage_name = Tournament::DIVISION_NAMES[current_stage_number]
      create_group_with_teams(stage_name, team_group)
    end
  end

  def create_play_offs
    stage_name = Tournament::PLAY_OFF_NAMES[current_stage_number]
    best_teams = context.tournament.divisions.map(&:best_four_teams)
    create_group_with_teams(stage_name, best_teams)
  end

  def create_finals
    stage_name = Tournament::FINAL_NAMES[current_stage_number]
    winners = context.tournament.groups.last.winners
    create_group_with_teams(stage_name, winners)
  end

  def current_stage_number
    context.tournament.current_stage_groups.size
  end

  def next_stage_accepted?(tournament)
    tournament.groups.first.try(:games).try(:size) != 1 and tournament.groups.first(2).all?(&:finished?)
  end

  def create_group_with_teams(name, teams)
    group = Group.new(tournament: context.tournament,
                      stage: context.tournament.stage,
                      name: name)
    group.save!
    group.teams << teams
  end
end

class CreateJoinTableGroupsTeams < ActiveRecord::Migration[5.2]
  def change
    create_join_table :groups, :teams do |t|
      t.index [:group_id, :team_id]
      t.index [:team_id, :group_id]
    end
  end
end

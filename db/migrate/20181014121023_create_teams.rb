class CreateTeams < ActiveRecord::Migration[5.2]
  def change
    create_table :teams do |t|
      t.string :name, null: false
      t.integer :score, default: 0
      t.references :tournament, index: true, foreign_key: true, null: false
      t.timestamps
    end
  end
end

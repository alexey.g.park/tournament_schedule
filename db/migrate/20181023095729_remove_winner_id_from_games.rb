class RemoveWinnerIdFromGames < ActiveRecord::Migration[5.2]
  def change
    remove_reference :games, :winner
  end
end

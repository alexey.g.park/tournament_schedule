class CreateGames < ActiveRecord::Migration[5.2]
  def change
    create_table :games do |t|
      t.references :group, index: true, foreign_key: true, null: false
      t.references :winner

      t.timestamps
    end

    add_foreign_key :games, :teams, column: :winner_id, primary_key: :id
  end
end

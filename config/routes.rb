Rails.application.routes.draw do
  root 'welcome#index'

  resources :tournaments, only: [:new, :create] do
    resources :groups, only: :create do
      post 'generate_results', on: :member
      # resources :results, only: :create
    end
  end
  resources :fakes, only: :create


  delete 'clean', to: 'clean#destroy'
end
